import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { PictureComponent } from './picture/picture.component';
import { HeaderComponent } from '../layouts/header/header.component';

@NgModule({
  imports: [CommonModule],
  declarations: [HomeComponent, PictureComponent],
  exports: [],
})
export class HomeModule {}
