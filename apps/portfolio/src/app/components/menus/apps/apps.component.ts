import { Component, OnInit } from '@angular/core';

interface Menu {
  link: string,
  img: string,
  name: string,
  code: string
}

@Component({
  selector: 'portfolio-menus-apps',
  templateUrl: './apps.component.html',
  styleUrls: ['./apps.component.scss']
})
export class AppsComponent implements OnInit {


  private menu : Menu[] = [
    {
      name: 'Portfolio',
      code: 'portfolio',
      img: '',
      link: 'https://daryl-cluzel.tech/Portfolio'
    },
    {
      name: 'D\'Signed',
      code: 'dsigned',
      img: '',
      link: 'https://daryl-cluzel.tech/DSigned'
    },
    {
      name: 'React App',
      code: 'ReactApp',
      img: '',
      link: 'https://daryl-cluzel.tech/ReactApp'
    },
  ]

  constructor() { }

  ngOnInit() {
  }

  public getMenu(){
    return this.menu
  }

}
