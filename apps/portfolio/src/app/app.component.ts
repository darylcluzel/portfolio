import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Message } from '@daryl-cluzel.tech/api-interfaces';

@Component({
  selector: 'portfolio-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent {

  public isLoading;
  public apps;

  hello$ = this.http.get<Message>('/api/hello');
  constructor(private http: HttpClient) {
    this.isLoading = false;
  }
}
