import { NgModule, Component } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { ShellService } from './shell/shell.service';
import { ShellComponent } from './shell/shell.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  ShellService.childRoutes([
    {
      path: '',
      // loadChildren: () => import('./components/home/home.module').then(m => m.HomeModule),
      component: HomeComponent
    },
  ]),
  // Fallback when no prior route is matched
  { path: '**', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
