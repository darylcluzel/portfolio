import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { changeMenuItem, loadMenu } from 'libs/ui/src/lib/+state/menu/menu.actions';
import { getMenuEntities, getAllMenu } from '../../../../../libs/ui/src/lib/+state/menu/menu.selectors';
import { MenuService } from '../../../../../libs/ui/src/lib/menu/menu.service';

@Component({
  selector: 'portfolio-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {

  $menu = this.store.pipe(select(getMenuEntities))

  constructor(private store: Store, private service: MenuService) {
    this.store.dispatch(loadMenu({menu : this.service.apps}));
   }

  ngOnInit() {
    console.log(this.$menu);
  }

  onMenuChangeItem(menu, id) {
    this.store.dispatch(changeMenuItem({menu,id}))
  }

}
