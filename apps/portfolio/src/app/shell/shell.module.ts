import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShellComponent } from './shell.component';
import { UiModule } from 'libs/ui/src/lib/ui.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    UiModule
  ],
  declarations: [ShellComponent]
})
export class ShellModule { }
