import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GqlConfigService } from './config/graphql-config.service';

@Module({
  imports: [GraphQLModule.forRootAsync({
    useClass: GqlConfigService,
  })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
