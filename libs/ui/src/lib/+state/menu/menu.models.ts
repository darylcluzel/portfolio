import { Apps } from "@daryl-cluzel.tech/api-interfaces";

/**
 * Interface for the 'Menu' data
 */
export interface MenuEntity extends Apps {
  id: string | number; // Primary ID
}
