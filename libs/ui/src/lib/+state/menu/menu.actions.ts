import { createAction, props } from '@ngrx/store';
import { MenuEntity } from './menu.models';

export const loadMenu = createAction('[Menu] Load Menu', props<{menu: MenuEntity[]}>());

export const loadMenuSuccess = createAction(
  '[Menu] Load Menu Success',
  props<{ menu: MenuEntity[] }>()
);

export const loadMenuFailure = createAction(
  '[Menu] Load Menu Failure',
  props<{ error: any }>()
);

export const changeMenuItem = createAction(
  '[Menu] Change Menu Item',
  props<{menu: MenuEntity[], id: string | number}>()
)
