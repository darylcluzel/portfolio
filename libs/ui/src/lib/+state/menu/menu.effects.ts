import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { fetch } from '@nrwl/angular';

import * as fromMenu from './menu.reducer';
import * as MenuActions from './menu.actions';

@Injectable()
export class MenuEffects {
  loadMenu$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MenuActions.loadMenu),
      fetch({
        run: (action) => {
          console.log(action)
          // Your custom service 'load' logic goes here. For now just return a success action...
          return MenuActions.loadMenuSuccess({ menu: action.menu });
        },

        onError: (action, error) => {
          console.error('Error', error);
          return MenuActions.loadMenuFailure({ error });
        },
      })
    )
  );

  changeMenuItem$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MenuActions.changeMenuItem),
      fetch({
        run: (action) => {
          console.log(action);
          return MenuActions.changeMenuItem({ menu : [], id: ''})
        },

        onError : (actoion, error) => {
          console.error('Error', error);
          return MenuActions.loadMenuFailure({error})
        }
      })
    )
  )

  constructor(private actions$: Actions) {}
}
