import { TestBed, async } from '@angular/core/testing';

import { Observable } from 'rxjs';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { NxModule, DataPersistence } from '@nrwl/angular';
import { hot } from '@nrwl/angular/testing';

import { MenuEffects } from './menu.effects';
import * as MenuActions from './menu.actions';

describe('MenuEffects', () => {
  let actions: Observable<any>;
  let effects: MenuEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [
        MenuEffects,
        DataPersistence,
        provideMockActions(() => actions),
        provideMockStore(),
      ],
    });

    effects = TestBed.get(MenuEffects);
  });

  describe('loadMenu$', () => {
    it('should work', () => {
      actions = hot('-a-|', { a: MenuActions.loadMenu() });

      const expected = hot('-a-|', {
        a: MenuActions.loadMenuSuccess({ menu: [] }),
      });

      expect(effects.loadMenu$).toBeObservable(expected);
    });
  });
});
