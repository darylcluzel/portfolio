import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  MENU_FEATURE_KEY,
  State,
  MenuPartialState,
  menuAdapter,
} from './menu.reducer';

// Lookup the 'Menu' feature state managed by NgRx
export const getMenuState = createFeatureSelector<MenuPartialState, State>(
  MENU_FEATURE_KEY
);

const { selectAll, selectEntities } = menuAdapter.getSelectors();

export const getMenuLoaded = createSelector(
  getMenuState,
  (state: State) => state.loaded
);

export const getMenuError = createSelector(
  getMenuState,
  (state: State) => state.error
);

export const getAllMenu = createSelector(getMenuState, (state: State) =>
  selectAll(state)
);

export const getMenuEntities = createSelector(getMenuState, (state: State) =>
  selectEntities(state)
);

export const getSelectedId = createSelector(
  getMenuState,
  (state: State) => state.selectedId
);

export const getSelected = createSelector(
  getMenuEntities,
  getSelectedId,
  (entities, selectedId) => selectedId && entities[selectedId]
);
