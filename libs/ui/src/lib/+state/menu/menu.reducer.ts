import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as MenuActions from './menu.actions';
import { MenuEntity } from './menu.models';
import { loadMenu } from './menu.actions';

export const MENU_FEATURE_KEY = 'menu';

export interface State extends EntityState<MenuEntity> {
  selectedId?: string | number; // which Menu record has been selected
  loaded: boolean; // has the Menu list been loaded
  error?: string | null; // last known error (if any)
}

export interface MenuPartialState {
  readonly [MENU_FEATURE_KEY]: State;
}

export const menuAdapter: EntityAdapter<MenuEntity> = createEntityAdapter<
  MenuEntity
>();

export const initialState: State = menuAdapter.getInitialState({
  // set initial required properties
  error: null,
  loaded: false,
  selectedId: 1
});



const menuReducer = createReducer(
  initialState,
  on(MenuActions.loadMenu, (state) => ({
    ...state,
    loaded: false,
    error: null,
  })),
  on(MenuActions.loadMenuSuccess, (state, { menu }) =>
    menuAdapter.setAll(menu, { ...state, loaded: true })
  ),
  on(MenuActions.loadMenuFailure, (state, { error }) => ({ ...state, error })),
  on(MenuActions.changeMenuItem, (state, {menu, id}) => ({...state, menu , id, loadMenu}))
);

export function reducer(state: State | undefined, action: Action) {
  return menuReducer(state, action);
}
