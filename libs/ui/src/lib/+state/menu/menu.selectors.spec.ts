import { MenuEntity } from './menu.models';
import { State, menuAdapter, initialState } from './menu.reducer';
import * as MenuSelectors from './menu.selectors';

describe('Menu Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getMenuId = (it) => it['id'];
  const createMenuEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
    } as MenuEntity);

  let state;

  beforeEach(() => {
    state = {
      menu: menuAdapter.addAll(
        [
          createMenuEntity('PRODUCT-AAA'),
          createMenuEntity('PRODUCT-BBB'),
          createMenuEntity('PRODUCT-CCC'),
        ],
        {
          ...initialState,
          selectedId: 'PRODUCT-BBB',
          error: ERROR_MSG,
          loaded: true,
        }
      ),
    };
  });

  describe('Menu Selectors', () => {
    it('getAllMenu() should return the list of Menu', () => {
      const results = MenuSelectors.getAllMenu(state);
      const selId = getMenuId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelected() should return the selected Entity', () => {
      const result = MenuSelectors.getSelected(state);
      const selId = getMenuId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it("getMenuLoaded() should return the current 'loaded' status", () => {
      const result = MenuSelectors.getMenuLoaded(state);

      expect(result).toBe(true);
    });

    it("getMenuError() should return the current 'error' state", () => {
      const result = MenuSelectors.getMenuError(state);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
