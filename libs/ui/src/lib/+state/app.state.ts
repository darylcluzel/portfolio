import { MenuEntity } from './menu/menu.models';
export interface AppState {
  menu: ReadonlyArray<MenuEntity>;
}
