import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { changeMenuItem, loadMenu } from '../+state/menu/menu.actions';
import { MenuEntity } from '../+state/menu/menu.models';
import { getMenuEntities, getAllMenu, getSelected, getMenuState } from '../+state/menu/menu.selectors';

@Component({
  selector: 'portfolio-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  @Input() menu : MenuEntity[];
  @Output() readonly positionChange: EventEmitter<number> = new EventEmitter<number>();

  constructor(private store: Store) {
  }

  ngOnInit(): void {
    console.log(Object.keys(this.menu))
    console.log(this.menu)
  }

}
