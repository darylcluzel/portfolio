import { Injectable } from '@angular/core';
import { MenuEntity } from '../+state/menu/menu.models';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  public apps : MenuEntity[] = [
    {
      id:'1',
      title: 'D\'Signed',
      name: 'dsigned',
      description: 'Le site web de mon entreprise',
      img: '',
      class: 'menu-item',
      text: ''
    },
    {
      id:'2',
      title: 'Portfolio',
      name: 'dsgned',
      description: 'Découvrez qui je suis',
      img: '',
      class: 'menu-item',
      text: ''
    },
    {
      id:'3',
      title: 'Live',
      name: 'live',
      description: 'Retrouvez moi ici en stream',
      img: '',
      class: 'menu-item',
      text: ''
    }
  ]
constructor() { }

}
