export interface Apps {
  title: string;
  name: string;
  description: string;
  img?: any;
  class?: string | string[];
  extra?: any;
  text?: string;
  particles?: any;
}
